import { connect } from 'react-redux'
import ArrivalView from '../components/ArrivalView'
import { loadArrialInfo } from '../../../store/modules/arrival'

const mapDispatchToProps = dispatch => ({
  updateArrivalInfo: forBusStopNo => {
    dispatch(loadArrialInfo(forBusStopNo))
  },
})

const mapStateToProps = state => ({
  busStops: state.bus.busStops,
})

export default connect(mapStateToProps, mapDispatchToProps)(ArrivalView)
