import React from 'react'
import PropTypes from 'prop-types'
import TextField from 'material-ui/TextField'
import FlatButton from 'material-ui/FlatButton'

import { loginUser } from '../../../services'

export class LoginForm extends React.Component {
  static propTypes = {
    router: PropTypes.object.isRequired,
  }

  constructor (props) {
    super(props)

    this.state = {
      email: '',
      password: '',
      error: '',
    }

    this.login = () => {
      const { email, password } = this.state
      
      if (!email) {
        this.setState({ error: 'email is empty' })
        return
      }

      if (!password) {
        this.setState({ error: 'password is empty' })
        return
      }

      loginUser(email, password)
      .then(() => {
        // success
        this.setState({ error: '' })
        this.props.router.push('/')
      })
      .catch(err => {
        // error
        const error = err.data.error
        this.setState({ error })
      })
    }

    this.handleInputChange = (event, key) => {
      this.setState({
        [key]: event.target.value,
      })
    }
  }

  render () {
    return (
      <div className='login-route__auth-form' >
        <div style={{ fontSize: '2em', textAlign: 'center' }}>
          LOG IN
        </div>
        <div style={{ fontSize: '0.8em', color: 'red', textAlign: 'center' }}>{this.state.error}</div>
        <TextField
          fullWidth
          floatingLabelText='Email'
          type='email'
          onChange={event => this.handleInputChange(event, 'email')}
        />
        <TextField
          fullWidth
          floatingLabelText='Password'
          type='password'
          onChange={event => this.handleInputChange(event, 'password')}
        />
        <FlatButton label='Log in'
          fullWidth
          style={{ background: '#d6d6d6' }}
          onTouchTap={this.login}
        />
      </div>
    )
  }
}

export default LoginForm
