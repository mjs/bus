import { getUserInfo } from '../../services'

// constants
export const LOC_STATUS_INIT = 0
export const LOC_STATUS_UPDATING = 1
export const LOC_STATUS_SUCCESS = 9

const MOCK_GEOLOCATION_COORDS = {
  accuracy:28,
  altitude:0,
  altitudeAccuracy:0,
  heading: NaN,
  latitude:1.296991,
  longitude:103.772793,
  speed:NaN,
} // NUS central library :)

// redux thunks

export const loadUserResource = () => (dispatch, getState) => {
  return getUserInfo().then(({ data }) => {
    dispatch(loadUserInfo(data))
  })
}

// This function is mocked to guarantee us to always receive a location from this function at max 3 secs.
// Off course it is more complicated in real case.
export const updateGeolocation = () => (dispatch, getState) => {
  const { locationStatus } = getState().user
  if (locationStatus < 1) {
    // if location was marked as success before. do this update sliently.
    dispatch(setLocationUpdatingStatus(LOC_STATUS_UPDATING))
  }

  const mockGeo = {
    coords: MOCK_GEOLOCATION_COORDS,
    timestamp: Date.now(),
  }

  if ('geolocation' in navigator) {
    const options = {
      enableHighAccuracy: true,
      timeout: 3000,
      maximumAge: 0,
    }
    navigator.geolocation.getCurrentPosition(position => {
      dispatch(loadGeolocation(position || mockGeo)) // if the postion is undefined return mock data.
      dispatch(setLocationUpdatingStatus(LOC_STATUS_SUCCESS))
    }, err => {
      console.error(err)
      dispatch(loadGeolocation(mockGeo))
      dispatch(setLocationUpdatingStatus(LOC_STATUS_SUCCESS)) // let's assume success >.<
    },
    options)
  } else {
    // let's mock the geolocation
    dispatch(loadGeolocation(mockGeo))
    dispatch(setLocationUpdatingStatus(LOC_STATUS_SUCCESS))
  }
}

// actions

export const LOAD_INFO = 'USER_LOAD_INFO'
export const LOAD_GEOLOCATION = 'USER_LOAD_GEOLOCATION'
export const SET_LOCATION_UPDATING_STATUS = 'USER_SET_LOCATION_UPDATING_STATUS'

/* action generator */
const actionGen = (type, payload) => ({ type, payload })

// payload: <the-user-object>
export const loadUserInfo = payload => actionGen(LOAD_INFO, payload)

// payload: <the-location-object>
export const loadGeolocation = payload => actionGen(LOAD_GEOLOCATION, payload)

// payload: <the-status>
export const setLocationUpdatingStatus = payload => actionGen(SET_LOCATION_UPDATING_STATUS, payload)

const handleLoadInfo = (state, action) => {
  const user = action.payload
  return { ...state, ...user }
}

const handleLoadGeolocation = (state, action) => {
  const location = action.payload
  return { ...state, location }
}

const handleSetLocationUpdatingStatus = (state, action) => {
  const locationStatus = action.payload
  return { ...state, locationStatus }
}

const initialState = {
  locationStatus: 0,
}

const ACTION_HANDLERS = {
  [LOAD_INFO]: handleLoadInfo,
  [LOAD_GEOLOCATION]: handleLoadGeolocation,
  [SET_LOCATION_UPDATING_STATUS]: handleSetLocationUpdatingStatus,
}

export default function userReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
