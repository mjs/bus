import React from 'react'
import PropTypes from 'prop-types'
import { List, ListItem } from 'material-ui/List'

export class BusList extends React.Component {
  static propTypes = {
    location: PropTypes.object.isRequired,
    servicesAtStops: PropTypes.object,
    arrival: PropTypes.object,
  }

  renderArrivalInfo (info) {
    const nextInMin = Math.round(parseInt(info.next.duration_ms) / 60000)
    const subsInMin = Math.round(parseInt(info.subsequent.duration_ms) / 60000)

    const arrivingStyle = { color: 'green' }
    const numberStyle = { fontSize: '1.3em', margin: '0 2px', color: '#43423d' }
    const unavaliableStyle = { color: '#d6d6d6' }

    const next = nextInMin
      ? nextInMin <= 1
        ? <span style={arrivingStyle}>arriving</span>
        : <span><span style={numberStyle}>{nextInMin}</span>mins</span>
      : <span style={unavaliableStyle}>unavaliable</span>
    
    const subsequent = subsInMin
      ? subsInMin <= 1
        ? <span style={arrivingStyle}>arriving</span>
        : `${subsInMin} mins`
      : <span style={unavaliableStyle}>unavaliable</span>
    
    return <p>Next: {next} Subsequent: {subsequent}</p>
  }

  render () {
    const { id } = this.props.location.query
    const { servicesAtStops, arrival } = this.props

    const buses = servicesAtStops[id] || []
    const arrivalInfo = buses.map(x => arrival[x] || {
      no: x,
      next: {
        time: '',
        duration_ms: null,
      },
      subsequent: {
        time: '',
        duration_ms: null,
      },
    })

    return (
      <List>
        {arrivalInfo.map(info =>
          <ListItem
            disabled
            key={info.no}
            primaryText={info.no}
            secondaryText={this.renderArrivalInfo(info)}
            secondaryTextLines={2}
          />
        )}
      </List>
    )
  }
}

export default BusList
