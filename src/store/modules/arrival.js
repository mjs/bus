import { getArrivalForBusStop } from '../../services'

// redux thunks

export const loadArrialInfo = busStopNo => (dispatch, getState) => {
  return getArrivalForBusStop(busStopNo).then(({ services }) => {
    const arrialInfo = services.reduce((acc, val) => ({ ...acc, [val.no]: val }), {})
    dispatch(loadInfo(arrialInfo))
  })
}

// actions

export const LOAD_INFO = 'ARRIVAL_LOAD_INFO'

/* action generator */
const actionGen = (type, payload) => ({ type, payload })

// payload: <the-object-to-store>
// eg: { "142": {"no":"142","next":{"time":"2017-06-04T09:42:30+00:00","duration_ms":422169},"subsequent":{"time":"2017-06-04T09:51:08+00:00","duration_ms":940169}} }
export const loadInfo = payload => actionGen(LOAD_INFO, payload)

const handleLoadInfo = (state, action) => {
  const data = action.payload
  return { ...state, ...data }
}

const initialState = {}

const ACTION_HANDLERS = {
  [LOAD_INFO]: handleLoadInfo,
}

export default function arrialReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
