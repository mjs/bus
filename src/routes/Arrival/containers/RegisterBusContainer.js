import { connect } from 'react-redux'
import RegisterBus from '../components/RegisterBus'
import { userRegisterBusToStop } from '../../../store/modules/bus'

const mapDispatchToProps = dispatch => ({
  registerHandler: ({ busNo, busName, busStopNo }) => {
    const bus = {
      no: busNo,
      name: busName,
      type: '0',
      routes: 2,
      operator: 'Custom',
    }
    return dispatch(userRegisterBusToStop(bus, busStopNo))
  },
})

const mapStateToProps = state => ({
  busStops: state.bus.busStops,
})

export default connect(mapStateToProps, mapDispatchToProps)(RegisterBus)
