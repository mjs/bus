export const MOCK_LOGIN_CREDENTIAL = {
  email: 'mike@gmail.com',
  password: 'password',
}

export const MOCK_USER = {
  id: 1,
  name: 'Mike',
  email: 'mike@gmail.com',
  phone: '93651234',
}
