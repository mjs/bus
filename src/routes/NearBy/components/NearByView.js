import React from 'react'
import PropTypes from 'prop-types'
import { List, ListItem } from 'material-ui/List'
import CircularProgress from 'material-ui/CircularProgress'

export class NearByView extends React.Component {
  static propTypes = {
    router: PropTypes.object.isRequired,
    nearbyBusStops: PropTypes.arrayOf(PropTypes.shape({
      no: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
    })),
    updateGeoLocation: PropTypes.func.isRequired,
    waitingForLocationUpdating: PropTypes.bool,
  }

  constructor (props) {
    super(props)

    this.handleBusStopClick = busStop => {
      this.props.router.push(`/home/arrival?id=${busStop.no}`)
    }
  }

  componentDidMount () {
    this.props.updateGeoLocation()
    this._geoUpdater = setInterval(() => {
      this.props.updateGeoLocation()
    }, 60000) // refresh location every min?
  }

  componentWillUnmount () {
    clearInterval(this._geoUpdater)
  }

  render () {
    if (this.props.waitingForLocationUpdating) {
      return (
        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
          <CircularProgress size={60} thickness={7} />
          <p>updating your location...</p>
        </div>
      )
    }

    return (
      <List>
        {this.props.nearbyBusStops.map(busStop =>
          <ListItem
            key={busStop.no}
            primaryText={busStop.name}
            onTouchTap={() => this.handleBusStopClick(busStop)}
          />
        )}
      </List>
    )
  }
}

export default NearByView
