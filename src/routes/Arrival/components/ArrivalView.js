import React from 'react'
import PropTypes from 'prop-types'
import FlatButton from 'material-ui/FlatButton'
import ChevronLeft from 'material-ui/svg-icons/navigation/chevron-left'

import BusList from '../containers/BusListContainer'
import RegisterBus from '../containers/RegisterBusContainer'

export class ArrivalView extends React.Component {
  static propTypes = {
    location: PropTypes.object.isRequired,
    router: PropTypes.object.isRequired,
    updateArrivalInfo: PropTypes.func.isRequired,
    busStops: PropTypes.array,
  }

  constructor (props) {
    super(props)

    this.state = {
      error: '',
    }
  }

  componentDidMount () {
    const { id } = this.props.location.query
    if (!id) {
      this.setState({ error: 'missing bus stop id' })
      return
    }
    this.props.updateArrivalInfo(id)
    this._updater = setInterval(() => {
      this.props.updateArrivalInfo(id)
    }, 60000) // refresh location every min?
  }

  componentWillUnmount () {
    clearInterval(this._updater)
  }

  render () {
    const { id } = this.props.location.query
    const busStop = this.props.busStops.find(x => x.no === id)

    return (
      <div>
        <FlatButton
          fullWidth
          label='All near by stops'
          icon={<ChevronLeft />}
          onTouchTap={() => { this.props.router.push('/home/nearby') }}
        />
        <div style={{ fontSize: '0.8em', color: 'red', textAlign: 'center' }}>{this.state.error}</div>
        {busStop &&
          <div>
            <RegisterBus
              location={this.props.location}
            />
            <div style={{ padding: '1em', background: '#d6d6d6' }}>
              <div style={{ fontSize: '0.8em' }}>Bus arrival time for:</div>
              <div style={{ fontSize: '1.5em' }}>{busStop.no} : {busStop.name}</div>
            </div>
          </div>
        }
        <BusList
          location={this.props.location}
        />
      </div>
    )
  }
}

export default ArrivalView
