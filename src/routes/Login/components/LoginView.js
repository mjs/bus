import React from 'react'
import PropTypes from 'prop-types'
import LoginForm from './LoginForm'

export class LoginView extends React.Component {
  static propTypes = {
    router: PropTypes.object.isRequired,
  }

  render () {
    const { router } = this.props
    return (
      <div className='login-route__viewport'>
        <LoginForm
          router={router}
        />
      </div>
    )
  }
}

export default LoginView
