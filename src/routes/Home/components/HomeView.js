import React from 'react'
import PropTypes from 'prop-types'
import BusIcon from 'material-ui/svg-icons/maps/directions-bus'

export class HomeView extends React.Component {
  static propTypes = {
    initialiseUser: PropTypes.func.isRequired,
    initialiseBus: PropTypes.func.isRequired,
    router: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    children: PropTypes.node,
  }

  componentDidMount () {
    this.props.initialiseUser()
    .then(() => {
      if (this.props.location.pathname === '/home' || this.props.location.pathname === '/home/') {
        this.props.router.push('/home/nearby')
      }
    })
    .catch(err => {
      console.error(err)
    })
    this.props.initialiseBus()
    .catch(err => {
      console.error(err)
    })
  }

  render () {
    return (
      <div>
        <div style={{ textAlign: 'center', padding: '10px 0 0 0' }} >
          <BusIcon style={{ verticalAlign: 'middle', width: '32px', height: '32px' }} />
          <span style={{ fontSize: '24px' }}>Singapore Buses</span>
        </div>
        {this.props.children}
      </div>
    )
  }
}

export default HomeView
