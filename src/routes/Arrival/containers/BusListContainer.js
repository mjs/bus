import { connect } from 'react-redux'
import BusList from '../components/BusList'
import { loadArrialInfo } from '../../../store/modules/arrival'

const mapDispatchToProps = dispatch => ({
  updateArrivalInfo: forBusStopNo => {
    dispatch(loadArrialInfo(forBusStopNo))
  },
})

const mapStateToProps = state => ({
  busStops: state.bus.busStops,
  servicesAtStops: state.bus.servicesAtStops,
  arrival: state.arrival,
})

export default connect(mapStateToProps, mapDispatchToProps)(BusList)
