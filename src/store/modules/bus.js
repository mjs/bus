import { getBusStops, getBusServices, getBusStopsServices } from '../../services'

// redux thunks

export const loadBusResource = () => (dispatch, getState) => {
  return Promise.all([
    getBusStops().then(({ data }) => {
      dispatch(loadInfo({ busStops: data }))
    }),
    getBusServices().then(({ data }) => {
      dispatch(loadInfo({ busServices: data }))
    }),
    getBusStopsServices().then(({ data }) => {
      dispatch(loadInfo({ servicesAtStops: data }))
    }),
  ])
}

export const userRegisterBusToStop = (bus, busStopNo) => (dispatch, getState) => {
  const busServices = getState().bus.busServices
  const busStops = getState().bus.busStops
  const hasBusAlready = busServices.some(x => x.no === bus.no)
  const isBusStopValid = busStops.some(x => x.no === busStopNo)
  // We only add the bus to redux store
  // Promise is returned here to simulate an api call
  return new Promise((resolve, reject) => {
    if (!isBusStopValid) {
      reject(new Error('Invalid bus stop'))
    } else if (hasBusAlready) {
      reject(new Error('Bus number is already used'))
    } else {
      dispatch(addBusService(bus))
      dispatch(registerBusToStop({ busStopNo, busNo: bus.no }))
      resolve({ status: 'ok' })
    }
  })
}

// actions

export const LOAD_INFO = 'BUS_LOAD_INFO'
export const ADD_BUS_SERVICE = 'BUS_ADD_BUS_SERVICE'
export const REGISTER_BUS_TO_STOP = 'BUS_REGISTER_BUS_TO_STOP'

/* action generator */
const actionGen = (type, payload) => ({ type, payload })

// payload: <the-object-to-store>
// eg: { busStops: [{ ... }] }
export const loadInfo = payload => actionGen(LOAD_INFO, payload)

// payload: <bus-service-object-to-be-added-to-busServices-array>
// eg: {"no":"2","routes":2,"type":"0","operator":"go-ahead","name":"Changi Village Ter - New Bridge Rd Ter"}
export const addBusService = payload => actionGen(ADD_BUS_SERVICE, payload)

// payload: { busStopNo: <bus-stop-number: string>, busNo: <bus-number: string> }
// eg: { busStopNo: 10000, busNo: 123 }
export const registerBusToStop = payload => actionGen(REGISTER_BUS_TO_STOP, payload)

const handleLoadInfo = (state, action) => {
  const data = action.payload
  return { ...state, ...data }
}

const handleAddBusService = (state, action) => {
  const busService = action.payload
  return Object.assign({}, state, { busServices: state.busServices.concat([busService]) })
}

const handleRegisterBusToStop = (state, action) => {
  const { busStopNo, busNo } = action.payload
  // bus and bus stop have to exist before we perform the action
  if (!state.busStops.find(busStop => busStop.no === busStopNo) || !state.busServices.find(busService => busService.no === busNo)) {
    return state
  }
  return Object.assign({}, state, {
    servicesAtStops: {
      ...state.servicesAtStops,
      [busStopNo]: state.servicesAtStops[busStopNo].concat([busNo]),
    },
  })
}

const initialState = {
  busStops: [],
  busServices: [],
  servicesAtStops: {},
}

const ACTION_HANDLERS = {
  [LOAD_INFO]: handleLoadInfo,
  [ADD_BUS_SERVICE]: handleAddBusService,
  [REGISTER_BUS_TO_STOP]: handleRegisterBusToStop,
}

export default function busReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
