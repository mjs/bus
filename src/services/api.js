import axios from 'axios'

export const getArrivalForBusStop = busStopNo => {
  return axios.get(`https://arrivelah.herokuapp.com/?id=${busStopNo}`).then(res => res.data || {})
}
