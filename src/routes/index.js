// We only need to import the modules necessary for initial render
import CoreLayout from '../layouts/PageLayout/PageLayout'
import Home from './Home'
import Arrival from './Arrival'
import NearBy from './NearBy'
import Login from './Login'

/*  Note: Instead of using JSX, we recommend using react-router
    PlainRoute objects to build route definitions.   */

// mock function for user session checking
function isLoggedIn () {
  const expire = localStorage.getItem('expire')
  return expire > Date.now()
}

function requireAuth (nextState, replace) {
  if (!isLoggedIn()) {
    replace({
      pathname: '/login',
    })
  }
}

export const createRoutes = store => ({
  path        : '/',
  component   : CoreLayout,
  indexRoute  : { onEnter: (nextState, replace) => replace('/home') },
  childRoutes : [
    {
      path: 'home',
      component: Home,
      onEnter: requireAuth,
      childRoutes: [
        {
          path: 'nearby',
          component: NearBy,
        },
        {
          path: 'arrival',
          component: Arrival,
        },
      ],
    },
    {
      path: 'login',
      component: Login,
    },
  ],
})

export default createRoutes
