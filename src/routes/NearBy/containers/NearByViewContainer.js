import { connect } from 'react-redux'
import NearByView from '../components/NearByView'
import { updateGeolocation, LOC_STATUS_UPDATING } from '../../../store/modules/user'
import { measureDistance } from '../../../utils'

const NEAR_BY_DISTANCE = 500 // meters

const mapDispatchToProps = dispatch => ({
  updateGeoLocation: () => {
    dispatch(updateGeolocation())
  },
})

const mapStateToProps = state => {
  const waitingForLocationUpdating = state.user.locationStatus === LOC_STATUS_UPDATING
  const userLocation = state.user.location ? state.user.location.coords : null
  let nearbyBusStops = []
  if (userLocation) {
    const { latitude, longitude } = userLocation

    nearbyBusStops = state.bus.busStops.map(busStop => {
      const lat = parseFloat(busStop.lat)
      const lng = parseFloat(busStop.lng)
      return Object.assign({}, busStop, { distance: measureDistance(latitude, longitude, lat, lng) })
    })
    .filter(x => x.distance < NEAR_BY_DISTANCE)
    .sort((a, b) => a.distance - b.distance)
  }
  return { nearbyBusStops, waitingForLocationUpdating }
}

export default connect(mapStateToProps, mapDispatchToProps)(NearByView)
