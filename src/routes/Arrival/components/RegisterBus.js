import React from 'react'
import PropTypes from 'prop-types'
import Dialog from 'material-ui/Dialog'
import TextField from 'material-ui/TextField'
import FlatButton from 'material-ui/FlatButton'

export class BusList extends React.Component {
  static propTypes = {
    location: PropTypes.object.isRequired,
    registerHandler: PropTypes.func.isRequired,
    busStops: PropTypes.array,
  }

  constructor (props) {
    super(props)

    this.defaultState = {
      open: false,
      busNo: '',
      busName: '',
      error: '',
    }

    this.state = { ...this.defaultState }
  }

  handleInputChange = (event, key) => {
    this.setState({
      [key]: event.target.value,
    })
  }

  handleClose = () => {
    this.setState({ open: false })
  }

  handleOpen = () => {
    this.setState({ ...this.defaultState })
    this.setState({ open: true })
  }

  handleRegister = () => {
    const { id } = this.props.location.query
    const { busNo, busName } = this.state

    if (!busNo) {
      this.setState({ error: 'Bus No. is empty' })
      return
    }

    if (!busName) {
      this.setState({ error: 'Bus Name is empty' })
      return
    }

    this.props.registerHandler({ busNo, busName, busStopNo: id })
    .then(() => {
      this.handleClose()
    })
    .catch(err => {
      this.setState({ error: err.message })
    })
  }

  render () {
    const { id } = this.props.location.query
    const busStop = this.props.busStops.find(x => x.no === id)

    const actions = [
      <FlatButton
        label='Cancel'
        primary
        onTouchTap={this.handleClose}
      />,
      <FlatButton
        label='Submit'
        primary
        onTouchTap={this.handleRegister}
      />,
    ]

    return (
      <div>
        <FlatButton label='Register a bus to this stop'
          fullWidth
          onTouchTap={this.handleOpen}
        />
        <Dialog
          title={`Register a bus ${busStop ? 'to ' + busStop.no + ' : ' + busStop.name : ''}`}
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
        >
          <div style={{ fontSize: '0.8em', lineHeight: '1', height: '1em', color: 'red' }}>{this.state.error}</div>
          <TextField
            fullWidth
            floatingLabelText='Bus No.'
            onChange={event => this.handleInputChange(event, 'busNo')}
          />
          <TextField
            fullWidth
            floatingLabelText='Bus Name'
            onChange={event => this.handleInputChange(event, 'busName')}
          />
        </Dialog>
      </div>
    )
  }
}

export default BusList
