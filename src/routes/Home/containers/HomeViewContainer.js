import { connect } from 'react-redux'
import HomeView from '../components/HomeView'
import { loadBusResource } from '../../../store/modules/bus'
import { loadUserResource } from '../../../store/modules/user'

const mapDispatchToProps = dispatch => ({
  initialiseBus: () => {
    return dispatch(loadBusResource())
  },
  initialiseUser: () => {
    return dispatch(loadUserResource())
  },
})

const mapStateToProps = state => ({

})

export default connect(mapStateToProps, mapDispatchToProps)(HomeView)
